![Logo](https://gitlab.com/uploads/-/system/project/avatar/41594349/sock-puppet-clipart-4.png)

# SOCK PUPPET

Uses Puppeteer to create a headless browser app, that can log in, crawl multiple pages, then exports it to PDF.

## Features

- Pass authentication pop up
- Handle cookies
- Pass log in
- Handle multiple pages
- Export to PDF

#### Puppeteer

##### is a Node library which provides a high-level API to control headless Chrome or Chromium over the DevTools Protocol. It can also be configured to use full (non-headless) Chrome or Chromium.

[Read More](https://developer.chrome.com/docs/puppeteer/)

## Installation

Puppeteer

```bash
  npm install puppeteer
```

## Usage

Open imports/params.js and enter urls in array.

```javascript
  urls: ["	https://www.url1.com/	",
   "	https://www.url2.com/	",
   "	https://www.url3.com/	"
   ],
```

(optionally) enter a url for login, then provide the correct selectors in pdf.js and credentials in imports/credentials.js

## Deployment

```bash
  node pdf
```

## Authors

- [@GSEN](https://gitlab.com/GsenDev/)
