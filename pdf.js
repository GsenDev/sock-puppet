//ONLY WORKS IN HEADLESS MODE
const PARAMS = require("./imports/params");
const CREDENTIALS = require("./imports/credentials");

const puppeteer = require("puppeteer"); // include library

(async () => {
  // declare function
  const browser = await puppeteer.launch({ headless: true, slowMo: 0 }); // run browser
  const page = await browser.newPage(); // create new tab
  await page.setViewport({
    width: PARAMS.screenwidth,
    height: PARAMS.screenheight,
    deviceScaleFactor: 2,
  });
  await page.setDefaultNavigationTimeout(0);
  /*
  console.log("logging in...\n\n");
  //OPTIONAL
  // pass any authentication pop up
  await page.goto(PARAMS.loginurl, { waitUntil: "domcontentloaded" });

  await page.waitForTimeout(1000);

  //OPTIONAL
  //accept cookies
  try {
    const ACCEPT_SELECTOR = "#cookies-accept-btn";
    await page.click(ACCEPT_SELECTOR);
    await page.waitForTimeout(1000);

    console.log("accepting cookies....\n\n");
  } catch {
    console.log("no cookies panel found \n\n");
  }

  //OPTIONAL
  // pass second authentication
  const USERNAME_SEC_SELECTOR = "#signInEmailAddress";
  const PASSWORD_SEC_SELECTOR = "#signInPassword";
  const LOGINBTN_SEC_SELECTOR = "#logInButton";

  await page.waitForTimeout(4000);
  //headless browser logging in
  await page.click(USERNAME_SEC_SELECTOR);
  await page.keyboard.type(CREDENTIALS.username);

  await page.click(PASSWORD_SEC_SELECTOR);
  await page.keyboard.type(CREDENTIALS.password);

  await page.click(LOGINBTN_SEC_SELECTOR);

  
  //logged in
  await page.waitForNavigation();

  console.log("logged in...\n\n");
*/
  console.log("iterating URLs...\n\n");

  console.log(PARAMS.urls);

  //go to  each url, create a name from url, create pdf and write to folder
  for (const url of PARAMS.urls) {
    await page.setDefaultNavigationTimeout(0);
    await page.goto(url, { waitUntil: "domcontentloaded" });
    await page.waitForTimeout(3000);

    let short_url = url.replace(/\s+/g, "");
    short_url = short_url.substring(47, 150);
    short_url = short_url.replace(/\//g, "-");

    console.log("\n\n writing pdf " + short_url + " ..\n\n ");

    await page.emulateMediaType("screen");
    // use screen media
    //pdf will be exported to exports folder
    try {
      await page.pdf({
        format: "A3",
        path: "exports/" + short_url + ".pdf", // path to save pdf file
        displayHeaderFooter: false, // display header and footer (in this example, required!)
        printBackground: true, // print background
        landscape: false, // use horizontal page layout
      });
    } catch {
      console.log("make sure the folder exists");
    }
  }

  await browser.close(); // close browser

  console.log("Done");
})();
